# Unofficial bindings to eProsima Fast RTPS

[![Crates.io](https://img.shields.io/crates/v/fastrtps-sys.svg)](https://crates.io/crates/fastrtps-sys) [![Crates.io](https://img.shields.io/crates/d/fastrtps-sys.svg)](https://crates.io/crates/fastrtps-sys) [![license](https://img.shields.io/crates/l/qwerty.svg)](https://gitlab.com/starshell/middleware/fastrtps-sys/blob/master/LICENSE) [![Coverage Status](https://codecov.io/gl/starshell/middleware/fastrtps-sys/branch/master/graph/badge.svg)](https://codecov.io/gl/starshell/middleware/fastrtps-sys)

Linux: [![Build status](https://gitlab.com/starshell/middleware/fastrtps-sys/badges/master/pipeline.svg)](https://gitlab.com/starshell/middleware/fastrtps-sys/commits/master)
Windows: [![Build status](https://ci.appveyor.com/api/projects/status/k7ccce79080tfu18/branch/master?svg=true)](https://ci.appveyor.com/project/Eudoxier/fastrtps-sys/branch/master)

&nbsp;

--------------------------------------------------------------------------------

**This project is not affiliated with eProsima.**

--------------------------------------------------------------------------------

&nbsp;

Rust bindings for eProsima Fast RTPS (Real Time Publish Subscribe) protocol C++ implementation. RTPS is the wire interoperability protocol defined for the Data Distribution Service (DDS) by the Object Management Group (OMG) consortium.

## Usage

Add `fastrtps-sys` as a dependency in your `Cargo.toml` to use from crates.io:

```toml
[dependencies]
fastrtps-sys = "0.1.0"
```

Then add `extern crate fastrtps-sys;` to your crate root and run `cargo build` or `cargo update && cargo build` for your project. Detailed documentation for releases can be found on [docs.rs](https://docs.rs/fastrtps-sys/). This crate is not meant to be used directly but to provide only the wrapper and for other libraries to use and implement a rusty API on top.

## Contributing

The project is mirrored to GitHub, but all development is done on GitLab. Please use the [GitLab issue tracker](https://gitlab.com/starshell/middleware/fastrtps-sys). Don't have a GitLab account? Just email `incoming+starshell/middleware/fastrtps-sys@gitlab.com` and those emails automatically become issues (with the comments becoming the email conversation).

To contribute to **fastrtps-sys**, please see [CONTRIBUTING](CONTRIBUTING.md).

## License

**fastrtps-sys** is distributed under the terms of the **MPL 2.0** license. If this does not suit your needs for some reason please feel free to contact me, or open an issue.

See [LICENSE](LICENSE).
