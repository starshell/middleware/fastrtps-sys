extern crate bindgen;
extern crate cmake;

use cmake::Config;
use std::env;
use std::path::PathBuf;

fn main() {
    // Builds the project in the directory called `libfastrtps`,
    // and installs it into $OUT_DIR.
    let dst = Config::new("Fast-RTPS")
        .always_configure(true)
        .cflag("-std=c++11")
        .define("THIRDPARTY", "ON")
        .define("SECURITY", "ON")
        .build();

    // Tell cargo to tell rustc to link the generated fastrtps shared library.
    println!("cargo:rustc-link-search=native={}", dst.display());
    println!("cargo:rustc-link-lib=dylib=fastrtps");

    // The bindgen::Builder is the main entry point
    // to bindgen, and lets you build up options for
    // the resulting bindings.
    let bindings = bindgen::Builder::default()
        // The input header we would like to generate bindings for.
        .header("fastrtps_wrapper.hpp")
        // Add Include directory for header files.
        .clang_arg(format!("-I{}/include", dst.display()))
        // C++11 is required for Fast RTPS.
        .clang_arg("-std=c++11")
        // Finish the builder and generate the bindings.
        .generate()
        // Unwrap the Result and panic on failure.
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
